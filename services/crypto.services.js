const StellarSdk = require('stellar-sdk');
const _ = require('lodash');
const xdr = require('js-xdr');
const dotenv = require('dotenv');
dotenv.config();

const server = new StellarSdk.Server('https://horizon.stellar.org');
const sourceKeys = StellarSdk.Keypair
  .fromSecret(process.env.STELLAR_KEY);
const destinationId = 'GCELUB72DFDPJES7SQD2AQEVQQ6FWR4BYJ4KHHXIXIYPMQU2HWC3IJW6';
// Transaction will hold a built transaction we can resubmit if the result is unknown.
let transaction;

// First, check to make sure that the destination account exists.
// You could skip this, but if the account does not exist, you will be charged
// the transaction fee when the transaction fails.

const cryptoSend = (amount, address, memo) => {
  server.loadAccount(destinationId)
    // If the account is not found, surface a nicer error message for logging.
    .catch(function (error) {
      if (error instanceof StellarSdk.NotFoundError) {
        throw new Error('The destination account does not exist!');
      } else return error
    })
    // If there was no error, load up-to-date information on your account.
    .then(function() {
      return server.loadAccount(sourceKeys.publicKey());
    })
    .then(function(sourceAccount) {
      // Start building the transaction.
      transaction = new StellarSdk.TransactionBuilder(sourceAccount, {
        fee: StellarSdk.BASE_FEE,
        networkPassphrase: StellarSdk.Networks.PUBLIC
      })
        .addOperation(StellarSdk.Operation.payment({
          destination: destinationId,
          // Because Stellar allows transaction in many currencies, you must
          // specify the asset type. The special "native" asset represents Lumens.
          asset: StellarSdk.Asset.native(),
          amount: amount
        }))
        // A memo allows you to add your own metadata to a transaction. It's
        // optional and does not affect how Stellar treats the transaction.
        .addMemo(StellarSdk.Memo.text('2446'))
        // Wait a maximum of three minutes for the transaction
        .setTimeout(180)
        .build();
      // Sign the transaction to prove you are actually the person sending it.
      console.log(transaction, transaction.toEnvelope().toXDR('base64'));
      transaction.sign(sourceKeys);
      // And finally, send it off to Stellar!
      return server.submitTransaction(transaction);
    })
    .then(function(result) {
      console.log('Success! Results:', result);
    })
    .catch(function(error) {
      console.error('Something went wrong!', error);
      // If the result is unknown (no response body, timeout etc.) we simply resubmit
      // already built transaction:
      // server.submitTransaction(transaction);
    });
}

const getTransaction = async (memo) => {
  const book = await server.transactions()
    .forAccount('GCBR3TZZQWKQTSQD2A3IMXVXF5JK4BWSYZOHJ7JR7YRJRX7C7DU2UNO7')
    .order('desc')
    .call()
    .then(function(r) {
      const transaction = _.find(r.records, function(o) { return o.memo === memo; });
      const xdrToJson = JSON.stringify(StellarSdk.xdr.TransactionEnvelope
          .fromXDR(transaction.envelope_xdr, 'base64'));
          console.log(xdrToJson)
      const JsontoObject = JSON.parse(xdrToJson);
      const deposit = JsontoObject._value._attributes.tx._attributes.operations[0]
          ._attributes.body._value._attributes.amount.low / 10000000;
      console.log(deposit)
      return deposit;
    });

  return book;
}

module.exports = { cryptoSend, getTransaction };
