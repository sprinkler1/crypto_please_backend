const textToSpeech = require('@google-cloud/text-to-speech');
const fs = require('fs');
const util = require('util');

const makeMP3 = async (text, language, gender) => {
  const client = new textToSpeech.TextToSpeechClient();

  let escapedLines = text;
  escapedLines = escapedLines.replace(/&/g, '&amp;');
  escapedLines = escapedLines.replace(/"/g, '&quot;');
  escapedLines = escapedLines.replace(/</g, '&lt;');
  escapedLines = escapedLines.replace(/>/g, '&gt;');

  const expandedNewline = escapedLines.replace(/\n/g, '\n<break time="2s"/>');
  const ssml = '<speak>' + expandedNewline + '</speak>';

  const request = {
    input: {ssml: ssml},
    voice: {languageCode: 'ko-KR', name: 'ko-KR-Standard-A', ssmlGender: 'FEMALE'},
    audioConfig: {audioEncoding: 'MP3'},
  };

  const [response] = await client.synthesizeSpeech(request);

  return response.audioContent;
}

module.exports = { makeMP3 };
