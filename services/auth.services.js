const express = require('express');
const router = express.Router();
const User = require("../models/User");
const axios = require("axios");
const dotenv = require("dotenv");
dotenv.config();

const getUser = async (token, refreshToken) => {
  const userInfo = await axios({
    url: "https://id.twitch.tv/oauth2/userinfo",
    method: "get",
    headers: {
      "Authorization": "Bearer " + token
    }
  });

  const newUser = new User();
  newUser.username = userInfo.data.preferred_username;
  newUser.refreshToken = refreshToken;
  await newUser.save();
  
  return userInfo.data.preferred_username;
}

const getToken = async (accessCode) => {
  const res = await axios({
    url: "https://id.twitch.tv/oauth2/token?client_id="+process.env.CLIENT_ID+
          "&client_secret="+process.env.CLIENT_SECRET+"&code="+accessCode+
          "&grant_type=authorization_code&redirect_uri=http://localhost:8000/auth/twitch/callback",
    method: "post"
  });

  const username = await getUser(res.data.access_token, res.data.refresh_token);

  return username;
}

module.exports = { getToken, getUser };
