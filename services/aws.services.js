const dotenv = require('dotenv');
dotenv.config();
const fs = require('fs');
const AWS = require('aws-sdk');
const BUCKET_NAME = process.env.AWS_BUCKET_NAME;

const s3 = new AWS.S3(
  { 
    accessKeyId: process.env.AWS_ACCESSKEY_ID,
    secretAccessKey: process.env.AWS_SECERETACCESSKEY
  }
);

const uploadFile = async (file, fileName) => {  

  const params = {
    ACL: 'public-read',
    Bucket: BUCKET_NAME,
    Key: fileName+".mp3",
    Body: file
  }

  await s3.upload(params, function(err, data) {
    if (err) { throw err; }
    console.log("upload success")
  })
}

module.exports = { uploadFile };
