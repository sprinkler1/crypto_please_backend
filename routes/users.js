const express = require('express');
const router = express.Router();
const cors = require('cors');
const session = require('express-session');
const User = require("../models/User");

router.post('/', cors(), async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  const username = user.username;
  const address = user.xlmAddress || null;
  const memo = user.xlmMemo || null;
  const url = user.broadcastUrl || null;
  const deposit = user.deposit || 0;
  res.send({ username: username, address: address, memo: memo, deposit: deposit });
});

router.post('/wallet', cors(), async (req, res, next) => {
  console.log(req.body)

  const user = await User.findOne({ username: req.body.username });
  user.xlmAddress = req.body.wallet;
  user.xlmMemo = req.body.memo;
  user.save();

  res.send({ address: req.body.wallet, xlmMemo: req.body.memo });
});

module.exports = router;
