const express = require('express');
const router = express.Router();
const cors = require('cors');
const User = require("../models/User");
const { getToken, getUser } = require('../services/auth.services.js');

router.post("/twitch/callback", cors(), async function(req, res, next) {
  const response = await getToken(req.body.accessCode);
  
  res.send(response);
});

module.exports = router;
