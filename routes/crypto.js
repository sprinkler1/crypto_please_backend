const express = require('express');
const router = express.Router();
const cors = require('cors');
const { makeMP3 } = require('../services/tts.services.js')
const { uploadFile } = require('../services/aws.services.js');
const { cryptoSend, getTransaction } = require('../services/crypto.services.js');
const User = require("../models/User");
const app = require("../app.js");

router.post('/send', cors(), async (req, res, next) => {
  const io = req.app.get('socketio');
  const fileName = req.body.username + Math.floor(Math.random()*1000);
  const ttsAndUpload = async () => {
    const file = await makeMP3(req.body.message);

    await uploadFile(file, fileName);
  }

  await ttsAndUpload();

  const user = await User.findOne({ username: req.body.username });
  user.deposit -= Number(req.body.amount);
  user.save();
  await cryptoSend(req.body.amount, user.xlmAddress, user.xlmMemo);
  io.emit('donation', { text: req.body.message, url : fileName })  
  res.json({ url: fileName, text: req.body.message, deposit: user.deposit });
});

router.post('/deposit', cors(), async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  console.log(req.body.memo)
  const deposit = await getTransaction(req.body.memo);
  user.deposit += Number(deposit) - 0.01;
  user.save();

  res.json({ deposit: user.deposit });
})

module.exports = router;
