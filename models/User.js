const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: { type: String },
  refreshToken: { type: Object },
  xlmAddress: { type: String },
  xlmMemo: { type: String },
  broadcastUrl: { type: String},
  sponsors: { type: Object },
  deposit: { type: Number, default: 0 }
});

module.exports = mongoose.model('User', userSchema);
